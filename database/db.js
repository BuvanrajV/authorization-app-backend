const mysql = require("mysql2");
const { dbPort, database, host, password, user } = require("../config/config");

const db = mysql.createPool({
  connectionLimit: 100,
  port: dbPort,
  host,
  database,
  password,
  user,
});

module.exports = db;
