const bcrypt = require("bcrypt");
const db = require("../database/db");

const createUserInDB = (email, password) => {
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, 10, (err, hashPassword) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        db.query(
          "Insert into usersDetails (email,password) values (?,?)",
          [email, hashPassword],
          (err, res) => {
            if (err) {
              console.error(err);
              reject(err);
            } else {
              console.log(`email : ${email} , password : ${password}`);
              resolve(res);
            }
          }
        );
      }
    });
  });
};

const userLogin = (email, password) => {
  return new Promise((resolve, reject) => {
    db.query(
      "Select * from usersDetails where email=?",
      [email],
      (err, res) => {
        if (err || res.length === 0) {
          console.error(err);
          reject(err);
        } else {
          bcrypt.compare(password, res[0].password, (error, response) => {
            if (error) {
              console.log(error);
              reject(error);
            } else {
              resolve(response);
            }
          });
        }
      }
    );
  });
};

const getUserDetails = (email) => {
  return new Promise((resolve, reject) => {
    db.query(
      "select * from usersDetails where email=?",
      [email],
      (err, res) => {
        if (err) {
          console.error(err);
          reject(err);
        } else {
          resolve(res);
        }
      }
    );
  });
};

const updateDetailsInDB = (id, data) => {
  return new Promise((resolve, reject) => {
    db.query(`UPDATE usersDetails SET ? WHERE ID=?`, [data, id], (err, res) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        resolve(res);
      }
    });
  });
};

module.exports = {
  createUserInDB,
  userLogin,
  getUserDetails,
  updateDetailsInDB,
};
