const jwt = require("jsonwebtoken");
const {
  createUserInDB,
  userLogin,
  getUserDetails,
  updateDetailsInDB,
} = require("../models/usersDetailsModel");
const { accessTokenSecret } = require("../config/config");

const createUserController = (req, res) => {
  createUserInDB(req.body.email, req.body.password)
    .then(() => {
      res.send({ message: "Signup Successfully" });
    })
    .catch((err) => {
      if (err.message.includes("Duplicate entry")) {
        res.status(400).send({ message: "Email already exist" });
      } else {
        res.status(500).send({ message: err.message || "Some error occurred"});
      }
    });
};

const userLoginController = (req, res) => {
  let email = req.body.email;
  userLogin(email, req.body.password)
    .then((response) => {
      if (response) {
        const accessToken = jwt.sign(email, accessTokenSecret);
        res.send({ accessToken });
      } else {
        res.status(400).send({ message: "Wrong Password" });
      }
    })
    .catch(() => {
      res.status(400).send({
        message: "Wrong Email",
      });
    });
};

const getUserDetailsController = (req, res) => {
  jwt.verify(req.token, accessTokenSecret, (error, email) => {
    if (error) {
      console.error(error);
      res.status(400).send({ message: "Invalid Token" });
    } else {
      getUserDetails(email)
        .then((response) => {
          res.send(response);
        })
        .catch((err) => {
          res
            .status(500)
            .send({ message: err.message || "Some error occurred" });
        });
    }
  });
};

const updateUserDetailsController = (req, res) => {
  updateDetailsInDB(req.params.id, req.body)
    .then(() => {
      res.send({
        message: "Successfully updated",
      });
    })
    .catch((err) => {
      res.status(500).send({ message: err.message || "Some error occurred" });
    });
};

module.exports = {
  createUserController,
  userLoginController,
  getUserDetailsController,
  updateUserDetailsController,
};
