const bcrypt = require("bcrypt");

const checkSignupBody = (req, res, next) => {
  let email = req.body.email;
  if (Object.keys(req.body).length < 2) {
    res.status(400).send({
      message: "Email and Password can not be empty!",
    });
  } else if (!email.includes("." || "@") || /[A-Z]/.test(email)) {
    res.status(400).send({
      message: "Email should contain ( a-z . @ 1-9(optional) )",
    });
  } else {
    next();
  }
};

const checkLoginBody = (req, res, next) => {
  let email = req.body.email;
  if (Object.keys(req.body).length < 2) {
    res.status(400).send({
      message: "Email and Password can not be empty!",
    });
  } else if (!email.includes("." || "@") || /[A-Z]/.test(email)) {
    res.status(400).send({
      message: "Email Wrong",
    });
  } else {
    next();
  }
};

const checkToken = (req, res, next) => {
  const token = req.headers["authorization"].split(" ")[1];
  if (token === null) {
    res.status(400).send({
      message: "Token can not be empty",
    });
  } else {
    req.token = token;
    next();
  }
};

const checkBodyToUpdate = (req, res, next) => {
  if (Object.keys(req.body).length === 0) {
    res.status(400).send({
      message: "Content can not be empty!",
    });
  } else if (req.body.password) {
    bcrypt.hash(req.body.password, 10, (err, hashPassword) => {
      if (err) {
        console.error(err);
        reject(err);
      } else {
        req.body.password = hashPassword;
        next();
      }
    });
  } else {
    next();
  }
};

module.exports = {
  checkSignupBody,
  checkLoginBody,
  checkToken,
  checkBodyToUpdate,
};
