require("dotenv").config();

module.exports = { 
  port: process.env.PORT,
  accessTokenSecret: process.env.ACCESS_TOKEN_SECRET,
  dbPort: process.env.DB_PORT,
  database: process.env.DATABASE,
  host: process.env.HOST,
  password: process.env.PASSWORD,
  user: process.env.DB_USER,
};
