const express = require("express");
const {
  createUserController,
  userLoginController,
  getUserDetailsController,
  updateUserDetailsController,
} = require("../controller/userController");
const {
  checkSignupBody,
  checkLoginBody,
  checkToken,
  checkBodyToUpdate,
} = require("../middleware/middleware");
const router = express.Router();

router.post("/signup", checkSignupBody, createUserController);
router.post("/login", checkLoginBody, userLoginController);
router.get("/login", checkToken, getUserDetailsController);
router.put("/:id", checkBodyToUpdate, updateUserDetailsController);

module.exports = router;
