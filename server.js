const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const userRouter = require("./routes/userRoutes");
const { port } = require("./config/config");

const app = express();

app.use(bodyParser.json());
app.use(cors());

app.use("/user", userRouter);

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});
